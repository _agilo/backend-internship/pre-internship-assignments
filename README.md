# Pre-Internship

To attend the Agilo Backend Internship you should know the very basics of PHP. A good online course to introduce you to the basics is [PHP For Beginners | 3+ Hour Crash Course](https://www.youtube.com/watch?v=BUCiSSyIGGU). If you're not familiar with PHP we expect you to complete this course and learn the very basics and also complete the pre-internship assignment before coming in for an interview with us.

You can see the solutions for the course at the root of the repository at https://github.com/bradtraversy/php-crash/ and also for the Feedback App Project at https://github.com/bradtraversy/php-crash/tree/main/feedback/. If you're already familiar with the topics and code in these solution files you can skip directly to the pre-internship assignment.

## Pre-Internship Course

If you're not familiar with the very basics of PHP, you should watch and complete the [PHP For Beginners | 3+ Hour Crash Course](https://www.youtube.com/watch?v=BUCiSSyIGGU). The most important parts are:

- Output Constructs & Functions
- Data Types & Variables
- Arrays
- Conditionals
- Loops
- Functions
- Array Functions
- String Functions
- Feedback App Project

Please follow the instructions from the video and set up [XAMPP](https://www.apachefriends.org) (or some other local dev environment like [MAMP](https://www.mamp.info) or anything else) on your machine, follow the course and code along with the teacher.

Don't bother setting up Auto Reload / live-server from the video if you don't want to.

## Pre-Internship Assignment

In this assignment, you will extend the Feedback App from the [PHP For Beginners | 3+ Hour Crash Course](https://www.youtube.com/watch?v=BUCiSSyIGGU).

Feedback App consists of the [Feedback Form page](https://github.com/bradtraversy/php-crash/blob/main/feedback/index.php) and the [Feedback Listing page](https://github.com/bradtraversy/php-crash/blob/main/feedback/feedback.php).

Extend the Feedback App with this functionality:

- Display feedback ID on feedback items on the feedback listing page
- Add a rating field to the feedback form so users can also leave a rating
  - Make this a non-required field
  - Users should be able to enter rating values from 1 to 5
  - Ratings should display on the feedback listing page as stars
  - The number of stars corresponds to the rating selected
  - If the user didn't leave a rating display "no rating"
- Add a field to the feedback form so users can enter a URL to the video they're leaving the feedback for
  - Make this a required field
  - Display the URL on feedback items

The updated app should look like this:

- [Feedback Form page screenshot](screenshots/feedback-form-page.png)
- [Feedback Listing page screenshot](screenshots/feedback-listing-page.png)

Feel free to improve the Feedback App in any way you want for bonus points.

## Next Steps

Once you're done with the course and the assignment send us the work you've done. You can make a zip file of everything and send it in an email to internship@agilo.co (or upload everything to a repo on Github:).
